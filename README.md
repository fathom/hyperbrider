# Bridger

Painless public profiles for Ethereum accounts

------------------

## How it works

Bridger uses [hyperDB](https://github.com/mafintosh/hyperdb), a distributed
database built on an append-only log, to store data about Etheruem addresses.

It consists of two parts:

1. An on-chain [smart contract](./contracts/HyperEthRegistry.sol) that
   associates hyperDB keys with ethereum addresses.
2. A library for managing hyperDB instances and views onto them

A "view" is a function that takes a hyperDB instance and returns some data about
it. We use them to create queries on profile data. These queries can be simple
or complex, they can validate data or just return directl from the database.
This let's us construct a rich view of an identities profile. (For example,
validatio is very important when dealing with transactions on chain, you want to
make sure they exist!)

For example a user could associate a Github account to their ethereum address by
creating a gist containing their address. They would then store the link to the
gist in their hyperDB instance. A "Github" view would then lookup that link
fetch it's contents, and verify that the address contained matches the address
in the hyperDB instance.

## A demo

For a simple demo checkout [./src/demo.ts](./src/demo.ts)

To run it you have to first start up:

1. A local ethereum testnet with concepts deployed
2. A hyperdb bridge node

### Ethereum Testnet

You're going to need `truffle` and `ganache-cli` installed.

1. Start ganache with `ganache-cli`
2. in the `./contracts` run `truffle migrate`

### Bridge node
This one is easier, just run `npm run start`

---------

Once you've got that done run `npm run demo` and you should see it ask  you what
your name is! Check out the source to see what's going on.
