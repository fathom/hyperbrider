import {connectToGateway} from './connectToGateway'
import Web3 from 'web3'
import { Provider } from 'web3/types';
const hyperdb = require('hyperdb')
const ram = require('random-access-memory')
const RegistryContract = require('../contracts/build/contracts/ProfileRegistry.json')

// Main Class
export class Bridger {
  profiles: {
    [props: string]: Archive
  }

  gateway: string
  web3: Web3

  constructor(provider: Provider, gateway?: string) {
    this.profiles = {}
    this.web3 = new Web3(provider)
    this.gateway = gateway ? gateway : 'wss://dat-bridge.now.sh/archive/'
  }

  async fetchProfile(userAddress: string): Promise<boolean> {
    let networkID = await this.web3.eth.net.getId()
    if(!RegistryContract.networks[networkID]) throw new Error('No deployed registry on network')

    let address = RegistryContract.networks[networkID].address
    let abi = RegistryContract.abi
    let Registry = new this.web3.eth.Contract(abi, address)

    let deployTx = RegistryContract.networks[networkID].transactionHash
    let deployBlockNumber = (await this.web3.eth.getTransaction(deployTx)).blockNumber

    let ProfileEvents = await Registry.getPastEvents('profileUpdated', {
      filter: {user: userAddress},
      fromBlock: deployBlockNumber,
      toBlock: 'latest'
    })

    if(ProfileEvents.length === 0) return false

    let hyperDBkey = ProfileEvents.reduce((acc, event) => {
      if(event.blockNumber > acc.blockNumber) acc = event
      return acc
    }).returnValues._profile

    let archive = new hyperdb(ram, hyperDBkey)
    return new Promise<boolean>((resolve) => {
      archive.on('ready', () => {
        connectToGateway(archive, this.gateway)
        archive.get('/address', (err:Error, nodes:any) => {
          if(err) resolve(false)
          if(!nodes[0]) resolve(false)
          let address = nodes[0].toString('utf8')

          if(address !== userAddress) resolve(false)
          this.profiles[userAddress] = archive,
          resolve(true)
        })
      })})
  }

  async createProfile (userAddress:string, storage:RandomAccessStorage):Promise<Archive> {
    let accounts = await this.web3.eth.getAccounts()
    let networkID = await this.web3.eth.net.getId()

    if(!accounts.includes(userAddress)) throw new Error('Address not available in provider')
    if(!RegistryContract.networks[networkID]) throw new Error('No deployed registry on network')

    let address = RegistryContract.networks[networkID].address
    let abi = RegistryContract.abi
    let Registry = new this.web3.eth.Contract(abi, address)

    let archive:Archive = hyperdb(storage)

    return new Promise((resolve) => {
      archive.on('ready', async () => {
        let key = archive.key.toString('hex')
        connectToGateway(archive, this.gateway)
        connectToGateway(archive, this.gateway)
        archive.put('/address', userAddress, () =>{
          Registry.methods.addProfile(key).send({from: userAddress})
            .on('confirmation', () => {
              resolve(archive)
            })
        })
      })
    })
  }
}

// Types ----------------------

export type Archive = any
export type RandomAccessStorage = any
