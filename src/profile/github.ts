import {View, get} from './index'
import fetch from 'cross-fetch'

export const github:View<string> = {
  view: (archive):Promise<string|null> => {
    const regex = /^https:\/\/gist\.github\.com\/([\w]+)\/([\w]{32})\/?$/g
    return new Promise(async (resolve) => {
      let gistURL = await get('/github', archive)
      let address = await get('/address', archive)

      if(!address) return resolve(null)
      if(!gistURL) return resolve(null)

      let values = regex.exec(gistURL)
      if(!values) return resolve(null)

      let gistID = values[2]

      let githubAPIRequest = await fetch('https://api.github.com/gists/' + gistID)
      if (!githubAPIRequest.ok) return resolve(null)

      let gist = await githubAPIRequest.json()

      let gistAddress:string = gist.files[Object.keys(gist.files)[0]].content
      if (gistAddress.toLowerCase() !== address.toLowerCase()) return resolve(null)
      return resolve(gist.owner.login)
    })
  },

  write: (gistURL: string) => {
    return [
      {
        type: 'put',
        key: 'github',
        value: gistURL
      }
    ]
  }
}
