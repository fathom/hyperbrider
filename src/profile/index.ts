import {Archive} from '../bridger'
export {name} from './name'
export {github} from './github'

export interface View<T> {
  view: (archive: Archive) => Promise<T | null>,
  write: (...args:any[]) => Array<Write>
}

export type Write = {
  type: 'put',
  key: string,
  value: string | object
}

export function get (key: string, archive: Archive):Promise<string | null> {
  return new Promise((resolve, reject) => {
    archive.get(key, (err:Error, nodes:any) => {
      if(err) return reject(err)
      if(!nodes[0]) return resolve(null)
      resolve(nodes[0].value.toString('utf8'))
    })
  })
}
