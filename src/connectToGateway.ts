const websocket = require('websocket-stream')
const pump = require('pump')

export type Archive = any
export function connectToGateway (archive:Archive, URL:string) {
  const key = archive.key.toString('hex')
  const url = URL + key

  const socket = websocket(url)
  let archiveStream = archive.replicate({encrypt: false, live: true})
  pump(
    socket,
    archiveStream,
    socket,
    (err:Error) => {
      if (err) console.log(err)
    }
  )
}
