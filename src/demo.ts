import {Bridger} from './bridger'
import Web3 from 'web3'
import {name }from './profile'
let ram = require('random-access-memory')

let provider = new Web3.providers.HttpProvider('http://localhost:8545')

let gateway ='wss://dat-bridge-qdokuofert.now.sh/archive/'
// Instantiate bridger
let bridger = new Bridger(provider, gateway)

let main = async () => {
  let web3 = new Web3(provider)
  let account = (await web3.eth.getAccounts())[0]

  // create an new hyperdb instance and register it onchain
  let db = await bridger.createProfile(account, ram)

  // Use the write function from the NameView module to write to the db
  await new Promise((resolve) => {
    let writes = name.write('Jared')
    db.batch(writes, () => {
      resolve()
    })
  })
  //load the profile into bridger
  await bridger.fetchProfile(account)

  console.log(await name.view(bridger.profiles[account]))
}

main()
