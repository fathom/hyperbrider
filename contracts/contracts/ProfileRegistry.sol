pragma solidity ^0.4.24;
contract ProfileRegistry {
  event profileUpdated(address indexed user, string _profile);
  function addProfile(string _profile) public {
    emit profileUpdated(msg.sender, _profile);
  }
}
